class SessionsController < ApplicationController
  def new
  end

  def create # find user when POST form is submitted
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in user     # this function is defined in session helper
      redirect_to user

    else
      # Create an error message.
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

end
